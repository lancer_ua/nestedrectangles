//
//  ViewController.m
//  NestedRectangles
//
//  Created by Pavel Smirnov on 3/1/16.
//  Copyright © 2016 Pavel Smirnov. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)loadView
{
    [super loadView];
    NSLog(@"loadView");
}

#pragma mark - Main view method

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"viewDidLoad");
    
//    ### First task ###
//    [[self view] setBackgroundColor:[UIColor colorWithRed:(55.0/255.0)  \
//                                             green:(171.0/255.0) \
//                                              blue:(72.0/255.0)  \
//                                             alpha:1]];
    
//    ### Second task ###
    
//    UIView* rect1 = [[UIView alloc] initWithFrame:CGRectMake(10, 10, 300, 450)];
//    rect1.backgroundColor = [UIColor colorWithRed:redLevel green:greenLevel blue:blueLevel alpha:1.0];
//    [[self view] addSubview:rect1];
//    [rect1 release];
//    
//    UIView* rect2 = [[UIView alloc] initWithFrame:CGRectMake(20, 20, 280, 430)];
//    rect2.backgroundColor = [UIColor orangeColor];
//    [[self view] addSubview:rect2];
//    
//    UIView* rect3 = [[UIView alloc] initWithFrame:CGRectMake(30, 30, 260, 410)];
//    rect3.backgroundColor = [UIColor yellowColor];
//    [[self view] addSubview:rect3];
//    
//    UIView* rect4 = [[UIView alloc] initWithFrame:CGRectMake(40, 40, 240, 390)];
//    rect4.backgroundColor = [UIColor cyanColor];
//    [[self view] addSubview:rect4];
//    
//    UIView* rect5 = [[UIView alloc] initWithFrame:CGRectMake(50, 50, 220, 370)];
//    rect5.backgroundColor = [UIColor greenColor];
//    [[self view] addSubview:rect5];
//    
//    UIView* rect6 = [[UIView alloc] initWithFrame:CGRectMake(60, 60, 200, 350)];
//    rect6.backgroundColor = [UIColor blueColor];
//    [[self view] addSubview:rect6];
//    
//    UIView* rect7 = [[UIView alloc] initWithFrame:CGRectMake(70, 70, 180, 330)];
//    rect7.backgroundColor = [UIColor purpleColor];
//    [[self view] addSubview:rect7];
//    
//    UIView* rect8 = [[UIView alloc] initWithFrame:CGRectMake(80, 80, 160, 310)];
//    rect8.backgroundColor = [UIColor magentaColor];
//    [[self view] addSubview:rect8];
//    
//    UIView* rect9 = [[UIView alloc] initWithFrame:CGRectMake(90, 90, 140, 290)];
//    rect9.backgroundColor = [UIColor grayColor];
//    [[self view] addSubview:rect9];
//    
//    UIView* rect10 = [[UIView alloc] initWithFrame:CGRectMake(100, 100, 120, 270)];
//    rect10.backgroundColor = [UIColor lightGrayColor];
//    [[self view] addSubview:rect10];
    
    NSInteger rectanglesCount = 0;
    CGFloat spacingRectangles = 10;
    CGFloat rectangleX = 10;
    CGFloat rectangleY = 10;
    CGFloat rectangleWidth = 300;
    CGFloat rectangleHeight = 460;
    
    while (rectanglesCount < 10)
    {
        UIView* rectangle = [[UIView alloc] initWithFrame:CGRectMake(rectangleX, rectangleY, rectangleWidth, rectangleHeight)];
        CGFloat redLevel    = arc4random() % 256 / 256.0;
        CGFloat greenLevel  = arc4random() % 256 / 256.0;
        CGFloat blueLevel   = arc4random() % 256 / 256.0;
        
        rectangle.backgroundColor = [UIColor colorWithRed:redLevel green:greenLevel blue:blueLevel alpha:1.0];
        
        [[self view] addSubview:rectangle];

        rectangleX = rectangleX + spacingRectangles;
        rectangleY = rectangleY + spacingRectangles;
        rectangleWidth = rectangleWidth - (spacingRectangles * 2);
        rectangleHeight = rectangleHeight - (spacingRectangles * 2);
        
        rectanglesCount++;
        
        [rectangle release];
    }
}

#pragma mark - Views

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSLog(@"viewWillAppear");
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSLog(@"viewDidAppear");
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    NSLog(@"viewWillLayoutSubviews");
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    NSLog(@"viewDidLayoutSubviews");
}

#pragma mark - Memory

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
