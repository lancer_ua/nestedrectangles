//
//  Rectangle.h
//  NestedRectangles
//
//  Created by Pavel Smirnov on 3/1/16.
//  Copyright © 2016 Pavel Smirnov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Rectangle : UIView

//@property (nonatomic, readonly) UIColor *rectColor;

- (instancetype)initWithFrame:(CGRect)frame;

@end
