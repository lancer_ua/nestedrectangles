//
//  AppDelegate.h
//  NestedRectangles
//
//  Created by Pavel Smirnov on 3/1/16.
//  Copyright © 2016 Pavel Smirnov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

