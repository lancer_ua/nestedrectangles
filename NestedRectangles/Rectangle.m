//
//  Rectangle.m
//  NestedRectangles
//
//  Created by Pavel Smirnov on 3/1/16.
//  Copyright © 2016 Pavel Smirnov. All rights reserved.
//

#import "Rectangle.h"

@interface Rectangle ()

//@property (nonatomic, readwrite) UIColor *rectColor;

@end

@implementation Rectangle

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    CGFloat redLevel    = arc4random() % 256 / 256.0;
    CGFloat greenLevel  = arc4random() % 256 / 256.0;
    CGFloat blueLevel   = arc4random() % 256 / 256.0;
    
    self.backgroundColor = [UIColor colorWithRed:redLevel green:greenLevel blue:blueLevel alpha:1.0];
    
    return self;
}

- (void)drawNestedRectangles
{
    
    
}

@end
